# アペンダブル・プログラミング

Todeslangの主要なコンセプトに、「既存のコードを書き換えることなしに、末尾に新たなコードを追加することで、既存の機能を変更または拡張することができる。」というものがある。これを「アペンダブル・プログラミング」と呼ぶ。

題材として、0以上10 (十六進法) 未満の自然数を表示する関数を考える。そして、末尾にコードを追加することで、0以上10未満の偶数のみを表示するように、動作を変更する。

ここで、0以上10 (十六進法) 未満の自然数を表示する関数は _print0to10_ という名前で、以下のような定義である。

```
($action = print 0 to 10) ^and (^not $state);
[state: #10; ]

($action = print 0 to 10) ^and ($state = #10);
[
    counter: #0;
    state: #20;
]

($action = print 0 to 10) ^and ($state = #20);
[state: $counter < #10 ^then #30 ^else #50; ]

($action = print 0 to 10) ^and ($state = #30);
[
    do: [print; ^@, $counter, { }; ];
    state: #40;
]

($action = print 0 to 10) ^and ($state = #40);
[
    counter: $counter + #1;
    state: #20;
]

($action = print 0 to 10) ^and ($state = #50);
[echo; ^false; ]
```

この関数を使うには、例えば以下のようにすればよい。

```
($action = main); [print 0 to 10; ]
```

## 行番号を狙う

まず考えるのは、特定の行番号を狙ってプログラムを書き換えることである。すなわち、以下のようなコードを追加する。

```
($action = print 0 to 10) ^and ($state = #40);
[
    counter: $counter + #2;
    state: #20;
]
```

これで目的は達せられるが、行番号は変わりやすいという欠点がある。もとのプログラムが書き換えられて、行番号が変更されると、この方法では動作しなくなる。

## _plus_ プリミティブ演算を狙う

`$counter + #1` という2項演算は、`[action: plus; left: $counter; right: #1; ]` という複合式の糖衣構文である。そこで、 _plus_ プリミティブ演算を狙って、その動作を書き換えることを狙う。

まず、 _anchor_ 引数変換ディレクティブで、 _plus_ プリミティブ演算の本来の動作を保存する。

```
anchor;
    if: $action = plus without patch always plus 2;
    parameters: [action: plus; ];
```

続いて、 _print0to10_ 関数から呼ばれた _plus_ プリミティブ演算に限って、右辺が常に2になるようにする。

```
(#1 < [$$$action; size; ^false; ]) ^and ([$$$action; at; #1; ] = print 0 to 10) ^and ($action = plus);
[
    action: plus without patch always plus 2;
    left: $left;
    right: #2;
]
```

ここで、`$$$action` というコードは見た目がいかめしい。これは、歴代の _action_ 引数の値を、新しい方から順に並べた配列である。
そのため、先頭の要素である `[$$$action; at; #0; ]` は `$action` と同じであり、ここでは _plus_ になる。`[$$$action; at; #1; ]` はその次なので、 _print0to10_ になる。ただし、あらかじめ `$$$action` 配列の長さを調べることが必要であり、ここでは `#1 < [$$$action; size; ^false; ]` というコードがそれにあたる。

これで、偶数のみを表示するという目的が達せされる。

## _plus_ プリミティブ演算の _right_ 引数を狙う

さらに別解として、 _print0to10_ 関数から呼び出された _plus_ プリミティブ演算の右辺の値を2に固定することを考える。これには、以下のコードを追加すればよい。

```
(#1 < [$$$action; size; ^false; ])
    ^and ([$$$action; at; #1; ] = print 0 to 10)
    ^and ($action = plus)
    ^and (^not ($right = #2));
[
    right: #2;
]
```
